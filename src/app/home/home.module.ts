import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule, IonList } from '@ionic/angular';

import { HomePageRoutingModule } from './home-routing.module';

import { HomePage } from './home.page';
import { PlacePage } from '../place/place.page';
import { PlaceCardPage } from '../place/place-card/place-card.page';
import { BasketCardPage } from '../place/basket-card/basket-card.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule
  ],
  declarations: [HomePage, PlacePage, PlaceCardPage]
})
export class HomePageModule { }
