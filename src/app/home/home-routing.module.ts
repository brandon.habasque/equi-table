import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule, NavParams } from '@ionic/angular';

import { HomePage } from './home.page';

const routes: Routes = [
  {
    path: '',
    component: HomePage
  }
];

@NgModule({
  imports: [FormsModule, IonicModule, CommonModule,RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [NavParams]
})
export class HomePageRoutingModule {}
