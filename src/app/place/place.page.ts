import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { BasketPage } from '../basket/basket.page';

@Component({
  selector: 'app-place',
  templateUrl: './place.page.html',
  styleUrls: ['./place.page.scss'],
})
export class PlacePage implements OnInit {

  public paniers = [];
  constructor(public modalCtrl: ModalController) { }

  ngOnInit() {
    this.paniers = [
      { img: 'assets/img/panier-gourmand.jpg', name: 'Panier Gourmand', size: 'S', price: '5.00€' },
      { img: 'assets/img/panier-gourmand.jpg', name: 'Panier Gourmand', size: 'M', price: '8.00€' },
      { img: 'assets/img/panier-gourmand.jpg', name: 'Panier Gourmand', size: 'L', price: '12.00€' },
    ];
  }

  async presentModal() {
    const modal = await this.modalCtrl.create({
      component: BasketPage,
      cssClass: 'custom-class'
    });
    return await modal.present();
  }

}
