import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-basket-card',
  templateUrl: './basket-card.page.html',
  styleUrls: ['./basket-card.page.scss'],
})
export class BasketCardPage implements OnInit {

  @Input() img: string;
  @Input() name: string;
  @Input() size: string;
  @Input() price: string;

  constructor() { }

  ngOnInit() {
  }

}
