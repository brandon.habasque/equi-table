import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BasketCardPageRoutingModule } from './basket-card-routing.module';

import { BasketCardPage } from './basket-card.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BasketCardPageRoutingModule
  ],
  declarations: [BasketCardPage]
})
export class BasketCardPageModule {}
