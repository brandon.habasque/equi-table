import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BasketCardPage } from './basket-card.page';

const routes: Routes = [
  {
    path: '',
    component: BasketCardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BasketCardPageRoutingModule {}
