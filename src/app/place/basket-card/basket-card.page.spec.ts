import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BasketCardPage } from './basket-card.page';

describe('BasketCardPage', () => {
  let component: BasketCardPage;
  let fixture: ComponentFixture<BasketCardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasketCardPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BasketCardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
