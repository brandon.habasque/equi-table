import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';

import { PlacePage } from './place.page';

const routes: Routes = [
  {
    path: '',
    component: PlacePage
  }
];

@NgModule({
  imports: [FormsModule, IonicModule, CommonModule,RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PlacePageRoutingModule { }
