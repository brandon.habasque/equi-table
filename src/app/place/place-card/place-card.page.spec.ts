import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PlaceCardPage } from './place-card.page';

describe('PlaceCardPage', () => {
  let component: PlaceCardPage;
  let fixture: ComponentFixture<PlaceCardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaceCardPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PlaceCardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
