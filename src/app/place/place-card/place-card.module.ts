import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PlaceCardPageRoutingModule } from './place-card-routing.module';

import { PlaceCardPage } from './place-card.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PlaceCardPageRoutingModule
  ],
  declarations: []
})
export class PlaceCardPageModule { }
