import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PlaceCardPage } from './place-card.page';

const routes: Routes = [
  {
    path: '',
    component: PlaceCardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PlaceCardPageRoutingModule {}
