import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RecipeCardPageRoutingModule } from './recipe-card-routing.module';

import { RecipeCardPage } from './recipe-card.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RecipeCardPageRoutingModule
  ],
  declarations: [RecipeCardPage]
})
export class RecipeCardPageModule {}
