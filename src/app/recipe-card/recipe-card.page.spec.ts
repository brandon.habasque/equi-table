import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RecipeCardPage } from './recipe-card.page';

describe('RecipeCardPage', () => {
  let component: RecipeCardPage;
  let fixture: ComponentFixture<RecipeCardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecipeCardPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RecipeCardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
