import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-recipe-card',
  templateUrl: './recipe-card.page.html',
  styleUrls: ['./recipe-card.page.scss'],
})
export class RecipeCardPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
