import { Component, OnInit } from '@angular/core';
import { LoadingController, ModalController } from '@ionic/angular';
import { ProductPage } from '../product/product.page';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.page.html',
  styleUrls: ['./basket.page.scss'],
})
export class BasketPage implements OnInit {

  public imgSource = '';
  constructor(private modalCtrl: ModalController, private loadingCtrl: LoadingController) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.imgSource = 'assets/img/panier-gourmand.jpg';
  }

  async presentModal() {
    this.close();
    const modal = await this.modalCtrl.create({
      component: ProductPage,
      cssClass: 'modal-fullscreen'
    });
    modal.onDidDismiss().then(async () => {
      const loading = await this.loadingCtrl.create({
        message: 'réouverture ...'
      });
      await loading.present();
      const modalBooking = await this.modalCtrl.create({
        component: BasketPage,
        cssClass: 'modal-fullscreen'
      });
      await modalBooking.present().then(() => loading.dismiss());
    });
    await modal.present();
  }

  close() {
    this.modalCtrl.dismiss();
  }

}
