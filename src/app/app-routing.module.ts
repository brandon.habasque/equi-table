import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', loadChildren: () => import('./login/login.module').then(m => m.LoginPageModule) },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'place',
    loadChildren: () => import('./place/place.module').then(m => m.PlacePageModule)
  },
  {
    path: 'place-card',
    loadChildren: () => import('./place/place-card/place-card.module').then(m => m.PlaceCardPageModule)
  },
  {
    path: 'basket-card',
    loadChildren: () => import('./place/basket-card/basket-card.module').then(m => m.BasketCardPageModule)
  },
  {
    path: 'tabs',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('./settings/settings.module').then(m => m.SettingsPageModule)
  },
  {
    path: 'product',
    loadChildren: () => import('./product/product.module').then(m => m.ProductPageModule)
  },
  {
    path: 'recipe-card',
    loadChildren: () => import('./recipe-card/recipe-card.module').then(m => m.RecipeCardPageModule)
  }


];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
