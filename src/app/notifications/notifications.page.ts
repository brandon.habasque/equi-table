import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage implements OnInit {

  public isRead = true;

  constructor() { }

  ngOnInit() {
  }

  segmentChanged(event) {
    this.isRead = !this.isRead;
  }

}
