import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-product',
  templateUrl: './product.page.html',
  styleUrls: ['./product.page.scss'],
})
export class ProductPage implements OnInit {

  public isData = true;
  constructor(public modalCtrl: ModalController) { }

  ngOnInit() {
  }

  segmentChanged(event) {
    this.isData = !this.isData;
  }

  dismiss() {
    this.modalCtrl.dismiss();
  }

}
