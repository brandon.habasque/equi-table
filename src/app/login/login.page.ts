import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public id = '';
  public pwd = '';
  constructor(
    public loadingController: LoadingController,
    private route: Router,
    public alertController: AlertController
  ) { }

  ngOnInit() {
  }

  async log() {
    try {
      const loading = await this.loadingController.create({
        message: 'Connexion en cours...'
      });
      await loading.present().then(async () => {
        if (this.id === 'toto' && this.pwd === '1234') {
          loading.dismiss();
          this.route.navigateByUrl('tabs');
        } else {
          loading.dismiss();
          const alert = await this.alertController.create({
            header: 'Connexion impossible',
            message: 'Identifiants incorrectes',
            buttons: ['OK']
          });
          await alert.present();
        }
      });
    } catch (err) {

    }
  }

  async socialNetworkLog() {
    try {
      const loading = await this.loadingController.create({
        message: 'Connexion en cours...'
      });
      await loading.present().then(async () => {
        loading.dismiss();
        this.route.navigateByUrl('tabs');
      });
    } catch (err) {

    }
  }

}
