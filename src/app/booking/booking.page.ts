import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { BasketPage } from '../basket/basket.page';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.page.html',
  styleUrls: ['./booking.page.scss'],
})
export class BookingPage implements OnInit {

  public isPending = true;
  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {
  }

  segmentChanged(event) {
    this.isPending = !this.isPending;
  }

  async goToBasket() {
    const modalBooking = await this.modalCtrl.create({ component: BasketPage, showBackdrop: false, cssClass: 'modal-fullscreen' });
    modalBooking.present();
  }
}
